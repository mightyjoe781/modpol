--call all files in this directory

local localdir = modpol.topdir

--Users
dofile (localdir .. "/users/users.lua")

--orgs
dofile (localdir .. "/orgs/base.lua")
dofile (localdir .. "/orgs/requests.lua")

--interactions
dofile (localdir .. "/interactions/interactions.lua")

--modules
dofile (localdir .. "/modules/consent.lua")
