modpol.orgs.request_params = {
    add_org = 1,
    delete = 0,
    add_member = 1,
    remove_member = 1
}

-- ================================
-- creates a new process linked to a request id
function modpol.orgs:create_process(process_type, request_id)
    if not modpol.modules[process_type] then
        modpol.ocutil.log('Process type "' .. process_type .. '" does not exist')
        return
    end

    local empty_index = nil
    -- linear search for empty process slots (lazy deletion)
    for k, v in ipairs(self.processes) do
        if v == 'deleted' then
            empty_index = k
            break
        end
    end

    local index
    -- attempts to fill empty spots in list, otherwise appends to end
    if empty_index then
        index = empty_index
    else
        index = #self.processes + 1
    end

    -- retrieving requested module
    local module = modpol.modules[process_type]
    local new_process = module:new_process(index, request_id, self.id)

    self.processes[index] = new_process

    modpol.ocutil.log('Created process #' .. index .. ' - ' .. process_type .. ' for ' .. self.name)
    self:record('Created process #' .. index .. ' - ' .. process_type, 'add_process')

    return index
end

-- ===========================
-- adds a new pending action to the org's table
function modpol.orgs:add_pending_action(process_id, user)
    -- adds tables if they don't exist already
    self.pending[user] = self.pending[user] or {}
    -- flagging actual action
    self.pending[user][process_id] = true

    local message = 
    modpol.interactions.message(user, "New pending action in " .. self.name)
    modpol.ocutil.log("Added pending action to " .. user .. " in process #" .. process_id .. ' from ' .. self.name)
    self:record('Added pending action to ' .. user .. ' in process #' .. process_id, "add_pending_action")
end

-- ========================
-- removes a pending action from the org's table
function modpol.orgs:remove_pending_action(process_id, user)
    
    if self.pending[user] then
        self.pending[user][process_id] = nil
        modpol.ocutil.log("Removed pending action from " .. user .. " in process #" .. process_id .. ' from ' .. self.name)
        self:record('Removed pending action from ' .. user .. ' in process #' .. process_id, "del_pending_action")
    else
        modpol.ocutil.log("Could not remove pending action from " .. user .. " in process #" .. process_id)
    end
end

-- =====================
-- removes all pending actions for a given process id from all users
function modpol.orgs:wipe_pending_actions(process_id)
   for user in pairs(self.pending) do
      self.pending[user][process_id] = nil
   end
   modpol.ocutil.log("Removed all pending actions for process #" .. process_id)
   self:record('Removed all pending actions for process #' .. process_id, "del_pending_action")
end

-- ======================
-- returns a boolean to indicate whether a user has any active pending actions
function modpol.orgs:has_pending_actions(user)
    -- next() will return the next pair in a table
    -- if next() returns nil, the table is empty
    if not self.pending[user] then
        return false
    else
        if not next(self.pending[user]) then
            return false
        else 
            return true
        end
    end
end

-- ===========================
-- compares to requests to see if they are identical
function modpol.orgs.comp_req(req1, req2)
    -- compares request type
    if req1.type ~= req2.type then
        return false
    else
        -- comparing parameters
        -- we can assume the number of params is the same as this is checked in the make_request func
        for k, v in ipairs(req1.params) do
            if v ~= req2.params[k] then
                return false
            end
        end
    end
    return true
end

-- ===============================
-- returns string of all active requests
function modpol.orgs:list_request()
    local str
    for id, req in ipairs(self.requests) do
        if req ~= "deleted" then
            if str then 
                str = str .. '\n' .. req.type .. ' (' .. req.user .. ') '
            else 
                str = req.type .. ' (' .. req.user .. ') '
            end
        end
    end
    return str
end

-- ===============================
-- if the request was approved, the associated function is called, otherwise it is deleted
-- TODO Rather than hard-coding functions below, this should be given an arbitrary result function based on the request
function modpol.orgs:resolve_request(request_id, approve)

   -- wipe actions
   self:wipe_pending_actions(request_id)
   
   if approve then
      local request = self.requests[request_id]
      local p = request.params

        -- there's probably a way to clean this up, the issue is the varying number of commands
        -- ex: self['add_member'](self, 'member_name')
        -- not sure if this is safe, more testing to do

        -- self[request.type](self, p[1], p[2], p[3])

        if request.type == "add_org" then
            self:add_org(request.params[1], request.user)
        elseif request.type == "delete" then
            self:delete()
        elseif request.type == "add_member" then
            self:add_member(request.params[1])
        elseif request.type == "remove_member" then
            self:remove_member(request.params[1])
        end
        
    end

    self.processes[request_id] = "deleted"
    modpol.ocutil.log('Deleted process #' .. request_id)

    self.requests[request_id] = "deleted"
    modpol.ocutil.log("Resolved request #" .. request_id .. ' in ' .. self.name)

    self:record("Resolved request #" .. request_id, "resolve_request")
end

-- ================================
-- tries to make a request to the org
function modpol.orgs:make_request(request)
    -- makes sure the request has the valid number of parameters
    local num_params = modpol.orgs.request_params[request.type]

    if num_params == nil then 
        modpol.ocutil.log('Error in ' .. self.name .. ':make_request -> request type is invalid')
        return false 
    end
    
    -- request.params should match the num of params for that type
    if #request.params ~= num_params then
        modpol.ocutil.log('Error in ' .. self.name .. ':make_request -> request has invalid number of parameters') 
        return false 
    end

    -- checking to see if identical request already exists
    for k, v in ipairs(self.requests) do
        if self.comp_req(request, v) == true then
            modpol.ocutil.log('Error in ' .. self.name .. ':make_request -> request has already been made')
            return false
        end
    end

    -- checking to see if user is able to make request
    local requested_policy = self.policies[request.type]

    -- if not the instance org (instance's don't have parents)
    if self.id ~= 1 then
       local parent_policy = modpol.orgs.get_org(self.parent).policies[request.type]
       
        -- tries to use org's policy table, defers to parent otherwise
        if not requested_policy then
            modpol.ocutil.log(request.type .. ' policy not found, deferring to parent org')
            requested_policy = parent_policy

            if not parent_policy then
                modpol.ocutil.log('Error in ' .. self.name .. ':make_request -> parent policy undefined')
                return false
            end
        end
    
    -- fails if instance policy undefined
    else
        if not requested_policy then
            modpol.ocutil.log('Error in  ' .. self.name .. ':make_request -> policy undefined')
            return false
        end
    end

    -- make sure user is allowed to make request
    if requested_policy.must_be_member and not self:has_member(request.user) then
        modpol.ocutil.log('Error in ' .. self.name .. ':make_request -> user must be org member to make this request')
        return false
    end

    local empty_index = nil
    -- linear search for empty process slots (lazy deletion)
    for k, v in ipairs(self.requests) do
        if v == 'deleted' then
            empty_index = k
            break
        end
    end

    -- attempts to fill empty spots in list, otherwise appends to end
    local request_id = nil
    if empty_index then
        self.requests[empty_index] = request
        request_id = empty_index
    else
        table.insert(self.requests, request)
        request_id = #self.requests
    end

    modpol.interactions.message(request.user, "Request made by " .. request.user .. " to " .. request.type .. " in " .. self.name)
    modpol.ocutil.log("Request made by " .. request.user .. " to " .. request.type .. " in " .. self.name)
    self:record("Request made by " .. request.user .. " to " .. request.type, "make_request")

    -- launching process tied to this request
    local process_id = self:create_process(requested_policy.process_type, request_id)

    -- returns process id of processes launched by this request
    return process_id
end

-- wrapper for process:interact function, ensures that user actually has a pending action for that process
function modpol.orgs:interact(process_id, user)
    process = self.processes[process_id]
    if self.pending[user] then
        if self.pending[user][process_id] == true then
            process:interact(user)
        else
            modpol.ocutil.log("Cannot interact with process, user does not have a valid pending action")
        end
    else
        modpol.ocutil.log("Cannot interact with process, user does not have any pending actions")
    end

end

