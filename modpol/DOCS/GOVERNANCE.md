One administrator, @ntnsndr, holds ultimate decision-making power over the project. This is a temporary arrangement while we build trust as a community and adopt a more inclusive structure.

* **Autocracy** The administrator holds ultimate decision-making power in the community.
    * **Executive** The administrator is responsible for implementing—or delegating implementation of—policies and other decisions.
    * **Lobbying** If participants are not happy with the administrator's leadership, they may voice their concerns or leave the community.
* **Do-ocracy** Those who step forward to do a given task can decide how it should be done, in ongoing consultation with each other and the administrator.
    * **Membership** Participation is open to anyone who wants to contribute. The administrator can remove misbehaving participants at will for the sake of the common good.
* **Code of Conduct** Participants are expected to abide by the Contributor Covenant (contributor-covenant.org).

---

Created by [Nathan Schneider](https://gitlab.com/medlabboulder/modpol)

[![CommunityRule derived](https://communityrule.info/assets/CommunityRule-derived-000000.svg)](https://communityrule.info)   
[Creative Commons BY-SA](https://creativecommons.org/licenses/by-sa/4.0/)
