
-- ===================================================================
-- Function: modpol.list_users(org)
-- Overwrites function at /users.lua
-- Params:
-- if nil, lists instance members; if an org name, lists its members
-- Output: a table with names of players currently in the game
modpol.list_users = function(org)
    local users = {}
    if (org == nil) then -- no specified org; all players
       for _,player in ipairs(minetest.get_connected_players()) do
          local name = player:get_player_name()
          table.insert(users,name)
       end
    else -- if an org is specified
       if (modpol.orgs[org] ~= nil) then -- org exists
          users = modpol.orgs[org]["members"]
       end
    end
    return users
 end