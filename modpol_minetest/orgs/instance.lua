--add members to the instance, if they are not already there.

minetest.register_on_joinplayer(function(player)
    local p_name = player:get_player_name()
    modpol.instance:add_member(p_name)
end)
