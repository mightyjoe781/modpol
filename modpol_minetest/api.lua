--call all files in this directory



-- ===================================================================
-- Minetest Specific Overrides of modpol functions
-- ===================================================================


local localdir = minetest.get_modpath("modpol") .. "/modpol_minetest"

--Users
dofile (localdir .. "/overrides/users/users.lua")

--orgs
--dofile (localdir .. "/overrides/orgs/orgs.lua")

--interactions
dofile (localdir .. "/overrides/interactions/interactions.lua")

-- messaging functions
-- dofile (localdir .. "/overrides/processes/processes.lua")



-- ===================================================================
-- Minetest Chatcommands
-- ===================================================================
dofile (localdir .. "/chatcommands/chatcommands.lua")


-- ===================================================================
-- Minetest Specific code
-- ===================================================================


-- orgs
-- ===================

dofile (localdir .. "/orgs/instance.lua") --add players to the instance when they join.