-- ===================================================================
-- /storage-mod_storage.lua
-- Persistent storage via Minetest's mod_storage method
-- See https://dev.minetest.net/StorageRef

-- Loads content of stored orgs and ledger from mod_storage


--get modstorageref
local mod_storage = minetest.get_mod_storage()



modpol.load_storage = function()
   
   -- load orgs
   local stored_orgs = minetest.deserialize(mod_storage:get_string("orgs"))
   if (stored_orgs ~= nil) then
      modpol.orgs = stored_orgs
   end
   -- load ledger
   local stored_ledger = minetest.deserialize(mod_storage:get_string("ledger"))
   if (stored_ledger ~= nil) then
      modpol.ledger = stored_ledger
   end
   -- load old_ledgers
   local old_stored_ledgers = minetest.deserialize(mod_storage:get_string("old_ledgers"))
   if (stored_ledger ~= nil) then
      modpol.old_ledgers = old_stored_ledgers
   end
end

-- Stores content of current orgs and ledger to mod_storage
modpol.store_data = function()

   -- write to storage
   mod_storage:set_string("orgs", minetest.serialize(modpol.orgs))
   mod_storage:set_string("ledger", minetest.serialize(modpol.ledger))
   mod_storage:set_string("old_ledgers", minetest.serialize(modpol.old_ledgers))
end
